﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

namespace FileFinder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        CancellationTokenSource tokenSource;
        static ManualResetEventSlim mre = new ManualResetEventSlim(true);
        public delegate void InvokeDelegateForRoot(String name);
        public delegate void InvokeDelegateForChild(TreeNode node);
        private static int numberOfFiles;
        private static Stopwatch stopWatch = new Stopwatch();

        private async void startButton_Click(object sender, EventArgs e)
        {
            startButton.Enabled = false;
            stopButton.Enabled = true;
            pauseButton.Enabled = true;
            chooseDirectoryButton.Enabled = false;

            String directory = directoryTextBox.Text;
            String fileMask = fileMaskTextBox.Text;
            String textInFile = textInFileTextBox.Text;

            TreeNode root = new TreeNode();
            catalogTree.Nodes.Add(root);

            Progress<string> fileNumber = new Progress<string>(text => this.fileNumberLabel.Text = text);
            Progress<string> timeFromStart = new Progress<string>(text => this.timeFromStartLabel.Text = text);
            Progress<string> processedFiles = new Progress<string>(text => this.processedFilesListBox.Items.Add(text));

            tokenSource = new CancellationTokenSource();
            CancellationToken cancelToken = tokenSource.Token;

            try
            {
                stopWatch.Start();
                numberOfFiles = 0;
                await Task.Run(() => EnumerateAllFiles(directory, fileMask, textInFile, catalogTree.Nodes[0], processedFiles, fileNumber, timeFromStart, cancelToken, catalogTree), cancelToken);
                stopWatch.Stop();

                startButton.Enabled = true;
                stopButton.Enabled = false;
                pauseButton.Enabled = false;
                chooseDirectoryButton.Enabled = true;
            }
            catch (OperationCanceledException)
            {
                startButton.Enabled = true;
                stopButton.Enabled = false;
                pauseButton.Enabled = false;
                resumeButton.Enabled = false;
                chooseDirectoryButton.Enabled = true;
            }

        }


        private static void EnumerateAllFiles(string path, string fileMask, string textInFile,
         TreeNode root, IProgress<string> processedFiles, IProgress<string> fileNumber,
         IProgress<string> timeFromStart, CancellationToken cancelToken, TreeView treeView)
        {
            TreeNode child;
            if (root.Text.Equals(""))
            {
                treeView.Invoke(new InvokeDelegateForRoot((s) => root.Text = s), path.Remove(path.LastIndexOf('\\')));
                string[] directories = null;
                try
                {
                    directories = Directory.GetDirectories(path);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                if (directories != null)
                {
                    foreach (var directory in directories)
                    {
                        timeFromStart.Report(stopWatch.Elapsed.Seconds.ToString());
                        EnumerateAllFiles(directory, fileMask, textInFile, root, processedFiles, fileNumber, timeFromStart, cancelToken, treeView);
                    }
                }
                string[] files = null;
                try
                {
                    files = Directory.GetFiles(path, fileMask);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                if (files != null)
                {
                    foreach (var file in files)
                    {
                        processedFiles.Report(file);
                        fileNumber.Report((++numberOfFiles).ToString());
                        timeFromStart.Report(stopWatch.Elapsed.Seconds.ToString());
                        string fileText = File.ReadAllText(file);
                        if (fileText.IndexOf(textInFile) != -1)
                        {
                            TreeNode fileNode = new TreeNode(file.Substring(file.LastIndexOf('\\') + 1));
                            treeView.Invoke(new InvokeDelegateForChild((node) => root.Nodes.Add(node)), fileNode);
                        }
                    }
                }
            }
            else
            {
                child = new TreeNode(path.Substring(path.LastIndexOf('\\') + 1));
                treeView.Invoke(new InvokeDelegateForChild((node) => root.Nodes.Add(node)), child);
                string[] directories = null;
                try
                {
                    directories = Directory.GetDirectories(path);
                }
                catch
                {

                }

                if (directories != null)
                {
                    foreach (var directory in directories)
                    {
                        timeFromStart.Report(stopWatch.Elapsed.Seconds.ToString());
                        EnumerateAllFiles(directory, fileMask, textInFile, child, processedFiles, fileNumber, timeFromStart, cancelToken, treeView);
                    }
                }
                string[] files = null;
                try
                {
                    files = Directory.GetFiles(path, fileMask);
                }
                catch { }

                if (files != null)
                {
                    foreach (var file in files)
                    {
                        processedFiles.Report(file);
                        fileNumber.Report((++numberOfFiles).ToString());
                        timeFromStart.Report(stopWatch.Elapsed.Seconds.ToString());
                        string fileText = File.ReadAllText(file);
                        if (fileText.IndexOf(textInFile) != -1)
                        {
                            TreeNode fileNode = new TreeNode(file.Substring(file.LastIndexOf('\\') + 1));
                            treeView.Invoke(new InvokeDelegateForChild((node) => child.Nodes.Add(node)), fileNode);
                        }
                    }
                }
            }
            cancelToken.WaitHandle.WaitOne(TimeSpan.FromSeconds(1));
            cancelToken.ThrowIfCancellationRequested();
            mre.Wait();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            directoryTextBox.Text = ConfigurationManager.AppSettings["directory"];
            fileMaskTextBox.Text = ConfigurationManager.AppSettings["fileMask"];
            textInFileTextBox.Text = ConfigurationManager.AppSettings["text"];
            fileNumberLabel.Text = "0";
            timeFromStartLabel.Text = "";
        }

        private void fileMaskTextBox_TextChanged(object sender, EventArgs e)
        {
            System.Configuration.Configuration currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            currentConfig.AppSettings.Settings["fileMask"].Value = fileMaskTextBox.Text;
            currentConfig.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");

        }

        private void chooseDirectoryButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FBD = new FolderBrowserDialog();
            if (FBD.ShowDialog() == DialogResult.OK)
            {
                directoryTextBox.Text = FBD.SelectedPath;
            }
        }

        private void directoryTextBox_TextChanged(object sender, EventArgs e)
        {
            System.Configuration.Configuration currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            currentConfig.AppSettings.Settings["directory"].Value = directoryTextBox.Text;
            currentConfig.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        private void textInFileTextBox_TextChanged(object sender, EventArgs e)
        {
            System.Configuration.Configuration currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            currentConfig.AppSettings.Settings["text"].Value = textInFileTextBox.Text;
            currentConfig.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            tokenSource.Cancel();
            stopWatch.Stop();
        }

        private void resumeButton_Click(object sender, EventArgs e)
        {
            mre.Set();
            stopWatch.Start();
            pauseButton.Enabled = true;
            resumeButton.Enabled = false;
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            mre.Reset();
            stopWatch.Stop();
            resumeButton.Enabled = true;
            pauseButton.Enabled = false;
        }
    }
}