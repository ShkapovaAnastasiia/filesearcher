﻿namespace FileFinder
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.processedFilesListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fileMaskTextBox = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.directoryTextBox = new System.Windows.Forms.TextBox();
            this.startButton = new System.Windows.Forms.Button();
            this.chooseDirectoryButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.fileNumberLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.timeFromStartLabel = new System.Windows.Forms.Label();
            this.textInFileTextBox = new System.Windows.Forms.TextBox();
            this.stopButton = new System.Windows.Forms.Button();
            this.resumeButton = new System.Windows.Forms.Button();
            this.pauseButton = new System.Windows.Forms.Button();
            this.catalogTree = new System.Windows.Forms.TreeView();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // processedFilesListBox
            // 
            this.processedFilesListBox.FormattingEnabled = true;
            this.processedFilesListBox.Location = new System.Drawing.Point(145, 176);
            this.processedFilesListBox.Name = "processedFilesListBox";
            this.processedFilesListBox.Size = new System.Drawing.Size(378, 147);
            this.processedFilesListBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Критерии поиска";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Директория";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Шаблон имени";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Содержимое файла";
            // 
            // fileMaskTextBox
            // 
            this.fileMaskTextBox.Location = new System.Drawing.Point(132, 101);
            this.fileMaskTextBox.Name = "fileMaskTextBox";
            this.fileMaskTextBox.Size = new System.Drawing.Size(297, 20);
            this.fileMaskTextBox.TabIndex = 5;
            this.fileMaskTextBox.TextChanged += new System.EventHandler(this.fileMaskTextBox_TextChanged);
            // 
            // directoryTextBox
            // 
            this.directoryTextBox.Location = new System.Drawing.Point(132, 65);
            this.directoryTextBox.Name = "directoryTextBox";
            this.directoryTextBox.Size = new System.Drawing.Size(297, 20);
            this.directoryTextBox.TabIndex = 6;
            this.directoryTextBox.TextChanged += new System.EventHandler(this.directoryTextBox_TextChanged);
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(145, 342);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 7;
            this.startButton.Text = "Начать";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // chooseDirectoryButton
            // 
            this.chooseDirectoryButton.Location = new System.Drawing.Point(451, 62);
            this.chooseDirectoryButton.Name = "chooseDirectoryButton";
            this.chooseDirectoryButton.Size = new System.Drawing.Size(75, 23);
            this.chooseDirectoryButton.TabIndex = 8;
            this.chooseDirectoryButton.Text = "Выбрать";
            this.chooseDirectoryButton.UseVisualStyleBackColor = true;
            this.chooseDirectoryButton.Click += new System.EventHandler(this.chooseDirectoryButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 310);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Обрабатываемый файл";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(609, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Кол-во файлов";
            // 
            // fileNumberLabel
            // 
            this.fileNumberLabel.AutoSize = true;
            this.fileNumberLabel.Location = new System.Drawing.Point(728, 72);
            this.fileNumberLabel.Name = "fileNumberLabel";
            this.fileNumberLabel.Size = new System.Drawing.Size(35, 13);
            this.fileNumberLabel.TabIndex = 12;
            this.fileNumberLabel.Text = "label7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(609, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Время, c";
            // 
            // timeFromStartLabel
            // 
            this.timeFromStartLabel.AutoSize = true;
            this.timeFromStartLabel.Location = new System.Drawing.Point(728, 108);
            this.timeFromStartLabel.Name = "timeFromStartLabel";
            this.timeFromStartLabel.Size = new System.Drawing.Size(35, 13);
            this.timeFromStartLabel.TabIndex = 14;
            this.timeFromStartLabel.Text = "label9";
            // 
            // textInFileTextBox
            // 
            this.textInFileTextBox.Location = new System.Drawing.Point(132, 138);
            this.textInFileTextBox.Name = "textInFileTextBox";
            this.textInFileTextBox.Size = new System.Drawing.Size(297, 20);
            this.textInFileTextBox.TabIndex = 15;
            this.textInFileTextBox.TextChanged += new System.EventHandler(this.textInFileTextBox_TextChanged);
            // 
            // stopButton
            // 
            this.stopButton.Enabled = false;
            this.stopButton.Location = new System.Drawing.Point(412, 342);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 23);
            this.stopButton.TabIndex = 16;
            this.stopButton.Text = "Остановить";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // resumeButton
            // 
            this.resumeButton.Enabled = false;
            this.resumeButton.Location = new System.Drawing.Point(320, 342);
            this.resumeButton.Name = "resumeButton";
            this.resumeButton.Size = new System.Drawing.Size(86, 23);
            this.resumeButton.TabIndex = 18;
            this.resumeButton.Text = "Продолжить";
            this.resumeButton.UseVisualStyleBackColor = true;
            this.resumeButton.Click += new System.EventHandler(this.resumeButton_Click);
            // 
            // pauseButton
            // 
            this.pauseButton.Enabled = false;
            this.pauseButton.Location = new System.Drawing.Point(226, 342);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(88, 23);
            this.pauseButton.TabIndex = 19;
            this.pauseButton.Text = "Пауза";
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // catalogTree
            // 
            this.catalogTree.Location = new System.Drawing.Point(612, 134);
            this.catalogTree.Name = "catalogTree";
            this.catalogTree.Size = new System.Drawing.Size(331, 231);
            this.catalogTree.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(650, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Результат поиска";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 377);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.catalogTree);
            this.Controls.Add(this.pauseButton);
            this.Controls.Add(this.resumeButton);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.textInFileTextBox);
            this.Controls.Add(this.timeFromStartLabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.fileNumberLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chooseDirectoryButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.directoryTextBox);
            this.Controls.Add(this.fileMaskTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.processedFilesListBox);
            this.Name = "Form1";
            this.Text = "File searcher";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox processedFilesListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox fileMaskTextBox;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox directoryTextBox;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button chooseDirectoryButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label fileNumberLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label timeFromStartLabel;
        private System.Windows.Forms.TextBox textInFileTextBox;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Button resumeButton;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.TreeView catalogTree;
        private System.Windows.Forms.Label label7;
    }
}

